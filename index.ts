import express, { Express, Request, Response } from "express";
import { Error } from "mongoose";
const dotenv = require("dotenv");
const mongoose = require("mongoose");
dotenv.config();
const cors = require("cors");
const app = express();
app.use(express.json());

app.use(
    cors({
        origin: [
            "http://localhost:3000",
            "https://folder-frontend.vercel.app",
            "https://folder-frontend-fardinoa.vercel.app",
            "https://folder-frontend-git-main-fardinoa.vercel.app",
        ],
        credentials: true,
    })
);

mongoose
    .connect(
        `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0.8a7eo.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`,
        {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        }
    )
    .then(() => {
        console.log("database connected");
    })
    .catch((err: Error) => {
        console.log(err);
    });

const port = process.env.PORT;

app.get("/", (req: Request, res: Response) => {
    res.send("Express + TypeScript Server");
});

const folderRoutes = require("./src/routes/folder");

app.use(folderRoutes);

app.listen(port, () => {
    console.log(`[server]: Server is running at  localhost:${port}`);
});
