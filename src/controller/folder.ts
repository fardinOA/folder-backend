const Folder = require("../models/folder");
import { Express, Request, Response } from "express";
exports.createFolder = async (req: Request, res: Response) => {
    try {
        const folder = await Folder.create({ folderName: req.body.folderName });
        if (!folder)
            res.status(500).json({
                message: "can't create folder",
            });

        res.status(200).json({
            message: `${folder.folderName} created successfully`,
            folder,
        });
    } catch (error) {}
};

exports.getAllFolder = async (req: Request, res: Response) => {
    try {
        const folders = await Folder.find({ _id: req.body.ids });
        if (!folders)
            res.status(500).json({
                message: "can't get folders",
            });

        res.status(200).json({
            folders,
        });
    } catch (error) {}
};

exports.getAllFolderId = async (req: Request, res: Response) => {
    try {
        const folders = await Folder.distinct("_id", {
            folderName: "root",
        });

        if (!folders)
            res.status(500).json({
                message: "can't get folders",
            });

        res.status(200).json({
            folders,
        });
    } catch (error) {}
};

exports.addFolder = async (req: Request, res: Response) => {
    try {
        const sub = await Folder.create({ folderName: req.body.folderName });

        const folder = await Folder.findByIdAndUpdate(
            { _id: req.body.id },
            {
                $addToSet: { subFolders: sub._id },
            },
            {
                multi: true,
                new: true,
                runValidators: true,
                useFindAndModify: true,
            }
        );
        if (!folder || !sub)
            res.status(500).json({
                message: "can't create folder",
            });

        res.status(200).json({
            message: `${sub.folderName} created successfully`,
            folder,
        });
    } catch (error) {}
};

exports.deleteFolder = async (req: Request, res: Response) => {
    try {
        const isDelete = await Folder.findByIdAndDelete(req.params.id);

        if (!isDelete)
            res.status(500).json({
                message: "can't delete folder",
            });
        // console.log(folder);
        res.status(200).json({
            message: `folder delete successfully`,
        });
    } catch (error) {}
};
