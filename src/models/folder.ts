const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const folderSchema = new Schema(
    {
        folderName: {
            type: String,
            required: true,
        },
        subFolders: [{ type: mongoose.Schema.Types.ObjectId, ref: "Folder" }],
    },
    { timestamps: true }
);

module.exports = mongoose.model("Folder", folderSchema);
