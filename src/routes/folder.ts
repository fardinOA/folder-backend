const express = require("express");
const {
    createFolder,
    getAllFolder,
    getAllFolderId,
    addFolder,
    deleteFolder,
} = require("../controller/folder");
const router = express.Router();

router.post("/create-folder", createFolder);
router.post("/add-folder", addFolder);
router.post("/folders", getAllFolder);
router.get("/folder-id", getAllFolderId);
router.delete("/delete/:id", deleteFolder);

module.exports = router;
