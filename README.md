
# Hi, I'm Fardin Omor Afnan! 👋

This is a simple folder structure backend project. Hope you like it 😍
## Run the project on your machine

Clone git repository

```bash
 git clone https://gitlab.com/fardinOA/folder-backend
```

Install packeges

```bash
  npm install or yarn
```

Go to project folder

```bash
  cd folder-backend
```

Run locally

```bash
  npm run dev
```
## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`MONGO_USER`

`MONGO_PASSWORD`

`DB_NAME`


## Demo

https://folder-backend.vercel.app


## Support

For support, email afnan.eu.cse@gmail.com  

